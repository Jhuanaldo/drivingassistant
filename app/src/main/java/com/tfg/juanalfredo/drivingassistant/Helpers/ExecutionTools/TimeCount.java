package com.tfg.juanalfredo.drivingassistant.Helpers.ExecutionTools;

public class TimeCount {
    //Medida de los tiempos
    long time_start = 0, time_end = 0;

    public void startTimeCount() {
        time_start = System.currentTimeMillis();
    }

    public long stopTimeCount() {
        time_end = System.currentTimeMillis();
        return time_end - time_start;
    }
}
