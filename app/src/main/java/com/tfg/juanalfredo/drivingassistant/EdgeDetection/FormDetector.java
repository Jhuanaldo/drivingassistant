package com.tfg.juanalfredo.drivingassistant.EdgeDetection;

import com.tfg.juanalfredo.drivingassistant.Constants.HighwayConstants;
import com.tfg.juanalfredo.drivingassistant.Helpers.Util.Util;

import android.graphics.Bitmap;

import org.opencv.android.Utils;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.Point;
import org.opencv.core.Scalar;
import org.opencv.imgproc.Imgproc;

public class FormDetector {

    private static Scalar RED = new Scalar(0, 255, 0);
    private static Scalar GOLD = new Scalar(100, 100, 0);

    private static int i = 0;

    private static boolean calculateLeftSide = true;
    private static boolean calculateRightSide = true;

    private double destYL, destXL, origYL, origXL;
    private double destYR, destXR, origYR, origXR;

    //define in process line points
    private static Point greenStartL;
    private static Point greenEndL;
    private static Point greenStartR;
    private static Point greenEndR;

    private static Point tealStartL;
    private static Point tealEndL;
    private static Point tealStartR;
    private static Point tealEndR;

    private Point avgSlopeLineStart;
    private Point avgSlopeLineEnd;

    //m avg's
    private double mLAvg;
    private double mRAvg;
    private int counterML;
    private int counterMR;

//    private TimeCount time = new TimeCount();
    Point start, end;

    public Mat[] getLinesMatrix(Bitmap frame) {

        //Instancing Mat image
        Mat imageMat = new Mat(frame.getHeight(), frame.getWidth(), CvType.CV_8U, new Scalar(4));
        Mat HoughMat = new Mat(frame.getHeight(), frame.getWidth(), CvType.CV_8U, new Scalar(4));
        Utils.bitmapToMat(frame, imageMat);

        //Apply Canny
        Imgproc.cvtColor(imageMat, HoughMat, Imgproc.COLOR_RGB2GRAY, 4);
        Imgproc.Canny(HoughMat, HoughMat,
            HighwayConstants.CANNY_LOWTHRESHOLD,
            HighwayConstants.CANNY_LOWTHRESHOLD,
            HighwayConstants.CANNY_RATIO,
            true
        );

        //Line probabilistic detection
        Mat lines = new Mat();
        Imgproc.HoughLinesP(HoughMat, lines, 1,
            Math.PI / HighwayConstants.HOUGH_DEGREE,
            HighwayConstants.HOUGH_THRESHOLD,
            HighwayConstants.HOUGH_MINLINESIZE,
            HighwayConstants.HOUGH_LINEGAP
        );
        return new Mat[] { lines, imageMat };
    }

    public Point[] getLanes(Bitmap frame, Mat lines, Mat imageMat) {

        double[] vec;
        double x1, x2, y1, y2, m;
        i++;

        destYR = 0; destXR = 0;
        origYR = HighwayConstants.HEIGH; origXR = HighwayConstants.UPPER_LIMIT;
        origYL = 0; origXL = HighwayConstants.UPPER_LIMIT;
        destYL = HighwayConstants.HEIGH; destXL = 0;

        //m avg's
        mRAvg = 0; mLAvg = 0;
        counterMR = 0; counterML = 0;

        for (int x = 0; x < lines.rows(); x++) {
            vec = lines.get(x, 0);
            x1 = vec[0];
            x2 = vec[2];
            y1 = vec[1];
            y2 = vec[3];

            if (x2 - x1 == 0 || y2 - y1 == 0) continue;
            else m = ((y2 - y1) / (x2 - x1));

            start = new Point(x1, y1);
            end = new Point(x2, y2);

            if (x2 > HighwayConstants.HORIZON - HighwayConstants.HORIZON / 4
                || x1 > HighwayConstants.HORIZON - HighwayConstants.HORIZON / 4
                || doesLineCrossLayers(frame, x1, y1, x2, y2))
                continue;

            if (isLeftLine(m, x1, x2, y1, y2)) {
                if (HighwayConstants.INNER_DELIMITER_START_X_L < x1
                    && HighwayConstants.INNER_DELIMITER_START_X_L < x2
                    && HighwayConstants.INNER_DELIMITER_START_Y_L < y1
                    && HighwayConstants.INNER_DELIMITER_START_Y_L < y2
                    && HighwayConstants.INNER_DELIMITER_END_X_L > x1
                    && HighwayConstants.INNER_DELIMITER_END_X_L > x2
                    && HighwayConstants.INNER_DELIMITER_END_Y_L < y1
                    && HighwayConstants.INNER_DELIMITER_END_Y_L < y2
                    || y1 < HighwayConstants.HEIGH / 2) continue;
                if (!matchWithHaze(x2, y2, x1, y1, 0)) continue;

                mLAvg += m;
                counterML += 1;
                if (x1 < origXL && y1 > origYL) {
                    origXL = x1;
                    origYL = y1;
                }
                if (x2 > destXL && y2 < destYL) {
                    destXL = x2;
                    destYL = y2;
                }
            }
            else if (isRightLine(m, x1, x2, y1, y2)) {
                if (HighwayConstants.INNER_DELIMITER_START_X_R < x1
                    && HighwayConstants.INNER_DELIMITER_START_X_R < x2
                    && HighwayConstants.INNER_DELIMITER_START_Y_R < y1
                    && HighwayConstants.INNER_DELIMITER_START_Y_R < y2
                    && HighwayConstants.INNER_DELIMITER_END_X_R > x1
                    && HighwayConstants.INNER_DELIMITER_END_X_R > x2
                    && HighwayConstants.INNER_DELIMITER_END_Y_R > y1
                    && HighwayConstants.INNER_DELIMITER_END_Y_R > y2
                    || y2 > HighwayConstants.HEIGH / 2) continue;
                if (!matchWithHaze(x1, y1, x2, y2, 1)) continue;

                mRAvg += m;
                counterMR += 1;
                if (x1 < origXR && y1 < origYR) {
                    origXR = x1;
                    origYR = y1;
                }
                if (x2 > destXR && y2 > destYR) {
                    destXR = x2;
                    destYR = y2;
                }
            }
        }

        calculateDefinitiveLines();

        return new Point[] {
            greenStartR, greenEndR, tealStartR, tealEndR,
            greenStartL, greenEndL, tealStartL, tealEndL
        };
    }

//    public static Mat drawBorder(Point [] point){
//        Point startL = new Point(origXL, origYL);
//        Point endL = new Point(destXL, destYL);
//        Imgproc.line(imageMat, startL, endL, RED, 3);
//
//        Point startR = new Point(origXR, origYR);
//        Point endR = new Point(destXR, destYR);
//        Imgproc.line(imageMat, startR, endR, RED, 3);
//    }

    private boolean isLeftLine(double m, double x1, double x2, double y1, double y2) {
        return (calculateLeftSide
            && m < - 1
            && m > - 4
            && y2 > (HighwayConstants.HEIGH / 2)
            && y1 > (HighwayConstants.HEIGH / 2)
            && x1 > (HighwayConstants.WIDTH / 15));
    }

    private boolean isRightLine(double m, double x1, double x2, double y1, double y2) {
        return (calculateRightSide
            && m > 1
            && m < 4
            && y2 < (HighwayConstants.HEIGH / 2)
            && y1 < (HighwayConstants.HEIGH / 2));
    }

    private void calculateDefinitiveLines() {
        if (origXR != HighwayConstants.UPPER_LIMIT && calculateRightSide) {
            avgSlopeLineStart = new Point(origXR, origYR);
            mRAvg /= counterMR;
            avgSlopeLineEnd = new Point(HighwayConstants.HORIZON, (mRAvg
                * HighwayConstants.HORIZON) - (mRAvg * origXR) + origYR);
            tealStartR = avgSlopeLineStart;
            tealEndR = avgSlopeLineEnd;

            if(origYR != HighwayConstants.HEIGH && destXR != 0 && destYR != 0) {
                greenStartR = new Point(origXR, origYR);
                greenEndR = new Point(destXR, destYR);
            }
        }

        if (origXL != HighwayConstants.UPPER_LIMIT && calculateLeftSide) {
            avgSlopeLineStart = new Point(origXL, origYL);
            mLAvg /= counterML;
            avgSlopeLineEnd = new Point(HighwayConstants.HORIZON, (mLAvg
                * HighwayConstants.HORIZON) - (mLAvg * origXL) + origYL);
            tealStartL = avgSlopeLineStart;
            tealEndL = avgSlopeLineEnd;

            if(origYL != 0 && destXL != 0 && destYL != HighwayConstants.HEIGH) {
                greenStartL = new Point(origXL, origYL);
                greenEndL = new Point(destXL, destYL);
            }
        }
    }

    private boolean matchWithHaze(double x1, double y1, double x2, double y2, int side) {
        double pivotX;
        double pivotY;
        if (side == 0) {
            pivotX = HighwayConstants.PIVOT_X_R;
            pivotY = HighwayConstants.PIVOT_Y_R;
        }
        else{
            pivotX = HighwayConstants.PIVOT_X_L;
            pivotY = HighwayConstants.PIVOT_Y_L;
        }
        double mHaze = ((y2 - pivotY) / (x2 - pivotX));
        double m = Util.getSlope(x1, y1, x2, y2);
        return Util.diff(mHaze, m) < 1;
    }

    private boolean doesLineCrossLayers(Bitmap frame, double x1, double y1, double x2,
            double y2) {
//        Color cOrig = new Color(frame.getRGB((int)x1, (int)y1));
//        Color cDest = new Color(frame.getRGB((int)x2, (int)y2));
//        int[] colorsOrig = { cOrig.getRed(), cOrig.getGreen(), cOrig.getBlue() };
//        int[] colorsDest = { cDest.getRed(), cDest.getGreen(), cDest.getBlue() };
//
//        for (int i = 0; i < colorsOrig.length(); i++) {
//            if (Util.diff(colorsOrig[i], colorsDest[i]) > HighwayConstants.MAX_COLOR_DIFF) //50
//                return true;
//        }
        return false;
    }
}