package com.tfg.juanalfredo.drivingassistant.Helpers.ExecutionTools;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.util.Log;

import com.tfg.juanalfredo.drivingassistant.Constants.HighwayConstants;
import com.tfg.juanalfredo.drivingassistant.FragmentHandler.AutoFitTextureView;
import com.tfg.juanalfredo.drivingassistant.EdgeDetection.FormDetector;
import com.tfg.juanalfredo.drivingassistant.FragmentHandler.CameraHandler;
import com.tfg.juanalfredo.drivingassistant.Helpers.Util.AlertHandler;
import com.tfg.juanalfredo.drivingassistant.Helpers.Util.Util;

import org.opencv.core.Mat;
import org.opencv.core.Point;

import java.util.LinkedList;
public class BackgroundProcessing {

    //Managers
    private FormDetector formManager = new FormDetector();

    //Control
    private TimeCount timeCount = new TimeCount();
    private static Context mCameraHandler;
    private static boolean firstAccess = true;
    private static int semaphore = 1;

    public BackgroundProcessing(Activity cameraHandler) {
        this.mCameraHandler = cameraHandler;
    }

    public void workWith(final Activity activity, final AutoFitTextureView mTextureView,
                         final LinkedList<String> stateHistoryBuffer)
            throws OutOfMemoryError {

        final Thread t1 = new Thread(new Runnable() {
            @Override
            public void run() {

                timeCount.startTimeCount();

                Bitmap frame = mTextureView.getBitmap();
                if (firstAccess) new HighwayConstants(frame);
                firstAccess = false;
                Log.i("GETBITMAP ", timeCount.stopTimeCount() + " ms");

                semaphore = 1;

                Point[] laneCoordinates = laneDetection(frame);
                String state = whichState(laneCoordinates);
                stateHistoryBuffer.add(state);
                new AlertHandler().warnIfDeparture(stateHistoryBuffer, activity);
            }
        });
        if (semaphore == 1) {
            semaphore = 0;
            t1.start();
            Log.i("SEMAPHORE", "Wait until getting the Bitmap from preview");
        }

    }

    private Point[] laneDetection(Bitmap frame) {
        Mat[] rawData = formManager.getLinesMatrix(frame);
        Mat lines = rawData[0];
        Mat imageMat = rawData[1];

        return formManager.getLanes(frame, lines, imageMat);
    }

    private String whichState(Point[] laneCoordinates) {
        String leftSideCase = "NOT_EXISTING";
        String rightSideCase = "NOT_EXISTING";

        Point greenStartR = laneCoordinates[0];
        Point greenEndR = laneCoordinates[1];
        Point tealStartR = laneCoordinates[2];
        Point tealEndR = laneCoordinates[3];
        Point greenStartL = laneCoordinates[4];
        Point greenEndL = laneCoordinates[5];
        Point tealStartL = laneCoordinates[6];
        Point tealEndL = laneCoordinates[7];

        if (greenStartR != null && greenEndR != null) {
            rightSideCase = "NOT_MATCH";
            if (Util.diff(
                    Util.getSlope(greenStartR, greenEndR),
                    Util.getSlope(tealStartR, tealEndR))
                    < HighwayConstants.LINE_MATCHING_MINIMUM) {
                rightSideCase = "MATCH";
            }
        }
        if (greenStartL != null && greenEndL != null) {
            leftSideCase = "NOT_MATCH";
            if (Util.diff(
                    Util.getSlope(greenStartL, greenEndL),
                    Util.getSlope(tealStartL, tealEndL))
                    < HighwayConstants.LINE_MATCHING_MINIMUM) {
                leftSideCase = "MATCH";
            }
        }
        return state(rightSideCase, leftSideCase);
    }

    private String state(String rightSideCase, String leftSideCase) {
        String state = "0";
        if (leftSideCase == "MATCH" && rightSideCase == "MATCH") {
            state = "1";
        } else if (leftSideCase == "MATCH" && rightSideCase == "NOT_MATCH") {
            state = "2.1";
        } else if (leftSideCase == "NOT_MATCH" && rightSideCase == "MATCH") {
            state = "2.2";
        } else if (leftSideCase == "NOT_MATCH" && rightSideCase == "NOT_EXISTING") {
            state = "3.1";
        } else if (leftSideCase == "NOT_EXISTING" && rightSideCase == "NOT_MATCH") {
            state = "3.2";
        } else if (leftSideCase == "MATCH" && rightSideCase == "NOT_EXISTING") {
            state = "4.1";
        } else if (leftSideCase == "NOT_EXISTING" && rightSideCase == "MATCH") {
            state = "4.2";
        } else if (leftSideCase == "NOT_EXISTING" && rightSideCase == "NOT_EXISTING") {
            state = "5";
        } else if (leftSideCase == "NOT_MATCH" && rightSideCase == "NOT_MATCH") {
            state = "6";
        }
        return state;
    }
}
