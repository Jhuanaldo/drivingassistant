package com.tfg.juanalfredo.drivingassistant.Helpers.Util;

import android.app.Activity;
import android.content.Context;
import android.media.MediaPlayer;
import android.os.Handler;
import android.os.SystemClock;


import com.tfg.juanalfredo.drivingassistant.R;

import java.util.Arrays;
import java.util.LinkedList;

public class AlertHandler {

    private static int maxNonSolutionAllowedStates = 5;
    private static String[] solutionStates = { "1", "2.1", "2.2", "4.1", "4.2" };

    public void warnIfDeparture(LinkedList<String> stateHistoryBuffer, Activity mCameraHandler) {
        for (int i = 0, ocurrences = 0; i < maxNonSolutionAllowedStates
                && stateHistoryBuffer.size() >= maxNonSolutionAllowedStates ; i++) {
            if (!Arrays.asList(solutionStates).contains(stateHistoryBuffer.get(i))) {
                ocurrences++;
            }
            if (ocurrences == maxNonSolutionAllowedStates) sendAlert(mCameraHandler);
        }
    }

    private void sendAlert(Activity mCameraHandler){
        MediaPlayer warning = MediaPlayer.create(mCameraHandler, R.raw.videoplayback);
        warning.start();
        SystemClock.sleep(1000);
        warning.release();
    }
}