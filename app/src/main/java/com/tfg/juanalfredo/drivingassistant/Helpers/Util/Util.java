package com.tfg.juanalfredo.drivingassistant.Helpers.Util;

import org.opencv.core.Point;

public class Util {

  public static double diff(double firtValue, double secondValue) {
    return Math.abs(firtValue - secondValue);
  }

  public static double getSlope(Point a, Point b) {
    double x1 = a.x, x2 = b.x;
    double y1 = a.y, y2 = b.y;
    return getSlope(x1, y1, x2, y2);
  }

  public static double getSlope(double x1, double y1, double x2, double y2) {
    double m;
    if (x2 - x1 == 0 || y2 - y1 == 0) m = 0;
    else m = ( (y2 - y1) / (x2 - x1) );
    return m;
  }
}
